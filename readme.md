* How to run the project

    1. Download and extract zip file.
    2.Create project into your system with name MyTenderProject.
    3.Replace your project src folder with the downloaded one.
    4.Run the Project with command 
        ng serve --open
        
* Software Requirements

    1.NodeJs.
    2.Angular CLI.
    3.Visual Studio code.
    
Note: Refer angular.io website for installation and configuration.